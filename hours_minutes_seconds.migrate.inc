<?php

/**
 * @file
 * Support for the Migrate API.
 */

/**
 * Migrate handler for HoursMinutesSeconds fields.
 *
 * This handler allows the import of values into these fields. You should
 * provide the integer (seconds) value in the mapping.
 */
class HoursMinutesSecondsMigrateHandler extends MigrateSimpleFieldHandler {

  /**
   * This does all the work. Mainly register the field type.
   */
  public function __construct() {
    parent::__construct([
      'value_key' => 'value',
      'skip_empty' => FALSE,
    ]);
    $this->registerTypes(['hours_minutes_seconds']);
  }

}

/**
 * Let's tell migrate that we have the implementation above.
 *
 * Implements hook_migrate_api().
 */
function hours_minutes_seconds_migrate_api() {
  $api = [
    'api' => 2,
  ];
  return $api;
}
