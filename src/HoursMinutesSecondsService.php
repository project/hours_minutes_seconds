<?php

namespace Drupal\hours_minutes_seconds;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a service to handle various hms related functionality.
 *
 * @package Drupal\hours_minutes_seconds
 */
class HoursMinutesSecondsService implements HoursMinutesSecondsServiceInterface {

  /**
   * The module handler service.
   *
   * @var Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function addMultiSearchTokens($item) {
    return '/' . $item . '+/';
  }

  /**
   * {@inheritdoc}
   */
  public function arrayGetNestedValue(array &$array, array $parents, &$key_exists = NULL) {
    $ref = &$array;
    foreach ($parents as $parent) {
      if (is_array($ref) && array_key_exists($parent, $ref)) {
        $ref = &$ref[$parent];
      }
      else {
        $key_exists = FALSE;
        $null = NULL;
        return $null;
      }
    }
    $key_exists = TRUE;
    return $ref;
  }

  /**
   * {@inheritdoc}
   */
  public function factorMap($return_full = FALSE) {
    $factor = drupal_static(__FUNCTION__);
    if (empty($factor)) {
      $factor = [
        'w' => [
          'factor value' => 604800,
          'label single' => 'week',
          'label multiple' => 'weeks',
        ],
        'd' => [
          'factor value' => 86400,
          'label single' => 'day',
          'label multiple' => 'days',
        ],
        'h' => [
          'factor value' => 3600,
          'label single' => 'hour',
          'label multiple' => 'hours',
        ],
        'm' => [
          'factor value' => 60,
          'label single' => 'minute',
          'label multiple' => 'minutes',
        ],
        's' => [
          'factor value' => 1,
          'label single' => 'second',
          'label multiple' => 'seconds',
        ],
      ];
      $this->moduleHandler->alter('hour_minutes_seconds_factor', $factor);
    }

    if ($return_full) {
      return $factor;
    }

    // We only return the factor value here.
    // for historical reasons we also check if value is an array.
    $return = [];
    foreach ($factor as $key => $val) {
      $value = (is_array($val) ? $val['factor value'] : $val);
      $return[$key] = $value;
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function formatOptions() {
    $format = drupal_static(__FUNCTION__);
    if (empty($format)) {
      $format = [
        'h:mm' => 'h:mm',
        'hh:mm:ss' => 'hh:mm:ss',
        'h:mm:ss' => 'h:mm:ss',
        'm:ss' => 'm:ss',
        'h' => 'h',
        'm' => 'm',
        's' => 's',
      ];
      $this->moduleHandler->alter('hour_minutes_seconds_format', $format);
    }
    return $format;
  }

  /**
   * {@inheritdoc}
   */
  public function formattedToSeconds($str, $format = 'h:m:s') {
    if (!strlen($str)) {
      return NULL;
    }

    if ($str == '0') {
      return 0;
    }

    $value = 0;

    // Is the value negative?
    $negative = FALSE;
    if (substr($str, 0, 1) == '-') {
      $negative = TRUE;
      $str = substr($str, 1);
    }

    $factorMap = $this->factorMap();
    $search = $this->normalizeFormat($format);

    for ($i = 0; $i < strlen($search); $i++) {
      // Is this char in the factor map?
      if (isset($factorMap[$search[$i]])) {
        $factor = $factorMap[$search[$i]];
        // What is the next seperator to search for?
        $bumper = '$';
        if (isset($search[$i + 1])) {
          $bumper = '(' . preg_quote($search[$i + 1], '/') . '|$)';
        }
        if (preg_match_all('/^(.*)' . $bumper . '/U', $str, $matches)) {
          // Replace , with .
          $num = str_replace(',', '.', $matches[1][0]);
          // Return error when found string is not numeric.
          if (!is_numeric($num)) {
            return FALSE;
          }
          // Shorten $str.
          $str = substr($str, strlen($matches[1][0]));
          // Calculate value.
          $value += ($num * $factor);
        }

      }
      elseif (substr($str, 0, 1) == $search[$i]) {
        // Expected this value, cut off and go ahead.
        $str = substr($str, 1);
      }
      else {
        // Does not follow format.
        return FALSE;
      }
      if (!strlen($str)) {
        // No more $str to investigate.
        break;
      }
    }

    if ($negative) {
      $value = 0 - $value;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeFormat($format) {
    $keys = array_keys($this->factorMap());
    $search_keys = array_map([$this, 'addMultiSearchTokens'], $keys);
    return preg_replace($search_keys, $keys, $format);
  }

  /**
   * {@inheritdoc}
   */
  public function secondsToFormatted($seconds, $format = 'h:mm', $leading_zero = TRUE) {

    // Return NULL on empty string.
    if ($seconds === '' || is_null($seconds)) {
      return NULL;
    }

    $factor = $this->factorMap();
    // We need factors, biggest first.
    arsort($factor, SORT_NUMERIC);
    $values = [];
    $left_over = $seconds;
    $str = '';
    if ($seconds < 0) {
      $str .= '-';
      $left_over = abs($left_over);
    }
    foreach ($factor as $key => $val) {
      if (strpos($format, $key) === FALSE) {
        // Not in our format, please go on, so we can plus this on a
        // value in our format.
        continue;
      }
      if ($left_over == 0) {
        $values[$key] = 0;
        continue;
      }
      $values[$key] = floor($left_over / $factor[$key]);
      $left_over -= ($values[$key] * $factor[$key]);
    }

    $format = explode(':', $format);

    foreach ($format as $key) {
      if (!$leading_zero && (empty($values[substr($key, 0, 1)]) || !$values[substr($key, 0, 1)])) {
        continue;
      }
      $leading_zero = TRUE;
      $str .= sprintf('%0' . strlen($key) . 'd', $values[substr($key, 0, 1)]) . ':';
    }
    if (!strlen($str)) {
      $key = array_pop($format);
      $str = sprintf('%0' . strlen($key) . 'd', 0) . ':';
    }
    return substr($str, 0, -1);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid($input, $format) {
    if ($this->formattedToSeconds($input, $format) !== FALSE) {
      return TRUE;
    }
    return FALSE;
  }

}
