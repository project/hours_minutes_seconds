<?php

namespace Drupal\hours_minutes_seconds;

/**
 * Interface for handling HoursMinutesSeconds functionality.
 *
 * This interface defines the methods related to manipulating and formatting
 * time in the Hours:Minutes:Seconds format.
 *
 * @package Drupal\hours_minutes_seconds
 */
interface HoursMinutesSecondsServiceInterface {

  /**
   * Get nested array values.
   *
   * @param array $array
   *   The array from which to fetch the nested value.
   * @param array $parents
   *   The keys to traverse to find the nested value.
   * @param null $key_exists
   *   A reference to the key existence flag.
   *
   * @return mixed
   *   The nested value, or NULL if not found.
   */
  public function arrayGetNestedValue(array &$array, array $parents, &$key_exists = NULL);

  /**
   * Returns possible format options.
   *
   * @return array
   *   An array of possible format options.
   */
  public function formatOptions();

  /**
   * Returns the factor map of the format options.
   *
   * Note: We cannot go further than weeks in this setup.
   * A month implies that we know how many seconds a month is.
   * However, months can have varying days (28-31).
   * The same applies to years (Y), quarters (Q), etc.
   *
   * Use HOOK_hour_minutes_seconds_factor_alter($factors) to modify factors.
   *
   * @param bool $return_full
   *   Whether to return the full factor map or a partial one.
   *
   * @return array
   *   An array representing the factor map.
   */
  public function factorMap($return_full = FALSE);

  /**
   * Returns number of seconds from a formatted string.
   *
   * @param string $str
   *   The formatted string (e.g., "2:30:45").
   * @param string $format
   *   The format of the input string.
   *
   * @return int
   *   The number of seconds represented by the formatted string.
   */
  public function formattedToSeconds($str, $format = 'h:m:s');

  /**
   * Returns a formatted string from the number of seconds.
   *
   * @param int $seconds
   *   The number of seconds.
   * @param string $format
   *   The desired format for the output (e.g., "h:mm").
   * @param bool $leading_zero
   *   Whether to include leading zeros in the formatted string.
   *
   * @return string
   *   The formatted time string.
   */
  public function secondsToFormatted($seconds, $format = 'h:mm', $leading_zero = TRUE);

  /**
   * Validate hour_minutes_seconds field input.
   *
   * @param int $input
   *   The input value (in seconds) to validate.
   * @param string $format
   *   The format of the value to validate.
   *
   * @return bool
   *   TRUE if the input is valid, FALSE otherwise.
   */
  public function isValid($input, $format);

  /**
   * Helper to normalize format.
   *
   * This method normalizes formats by changing double keys to single keys.
   *
   * @param string $format
   *   The format string to normalize.
   *
   * @return string
   *   The normalized format string.
   */
  public function normalizeFormat($format);

  /**
   * Helper to extend values in search array.
   *
   * @param string $item
   *   The item to add to the search array.
   *
   * @return mixed
   *   The extended search item.
   */
  public function addMultiSearchTokens($item);

}
